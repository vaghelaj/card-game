const express = require('express');
const path = require('path');
var philipsHueID = '/api/HU97BXcGaFXcb1cgclp8Fz3SnD07KtBizr7kxxvV/lights/';
const v3 = require('node-hue-api').v3;
const LightState = v3.lightStates.LightState;

const USERNAME = 'HU97BXcGaFXcb1cgclp8Fz3SnD07KtBizr7kxxvV'
    // The name of the light we wish to retrieve by name
    , LIGHT_ID = 1
;
const app = express();
app.use('/static', express.static('public'));

app.get('/', function(req, res){
    res.sendFile(path.join(__dirname + '/index.html'))
});

function lightwrong(req, res) {
v3.discovery.nupnpSearch()
    .then(searchResults => {
        const host = searchResults[0].ipaddress;
        return v3.api.createLocal(host).connect(USERNAME);
    })
    .then(api => {
        // Using a LightState object to build the desired state
        const state = new LightState()
            .on()
            .ct(200)
            .brightness(100)
            .rgb(255,0,0)
        ;

        return api.lights.setLightState(LIGHT_ID, state);
    })
    .then(result => {
        console.log(`Light state change was successful? ${result}`);
    });
};

app.listen(3000);